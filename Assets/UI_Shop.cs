using System.Collections;
using System.Collections.Generic;
using CodeMonkey.Utils;
using UnityEngine;
using UnityEngine.UI;

public class UI_Shop : MonoBehaviour
{
    private Transform container;
    private Transform shopItemTemplate;
    private static Dictionary<Item.ItemType, bool> equippedOrNotDic = new Dictionary<Item.ItemType, bool>();
    private static float totalCoinAmount;

    private void Awake()
    {
        container = transform.Find("container");
        shopItemTemplate = container.Find("shopItemTemplate");
        shopItemTemplate.gameObject.SetActive(false);
        totalCoinAmount = 100;
     }

    private void Start()
    {
        CreateItemButton(Item.ItemType.Armor, Item.GetSprite(Item.ItemType.Armor), "Armor", Item.GetCost(Item.ItemType.Armor), 0);
        equippedOrNotDic.Add(Item.ItemType.Armor, false);
        CreateItemButton(Item.ItemType.Helmet, Item.GetSprite(Item.ItemType.Helmet), "Helmet", Item.GetCost(Item.ItemType.Helmet), 1);
        equippedOrNotDic.Add(Item.ItemType.Helmet, false);
    }


    private void CreateItemButton(Item.ItemType itemType, Sprite itemSprite, string itemName, int itemCost, int positionIndex) 
    {
        Transform shopItemTransform = Instantiate(shopItemTemplate, container);
        RectTransform shopItemRectTransform = shopItemTransform.GetComponent<RectTransform>();
        shopItemTransform.gameObject.SetActive(true);

        float shopItemHeight = 200f;
        shopItemRectTransform.anchoredPosition = new Vector2(0, -shopItemHeight * positionIndex);

        shopItemTransform.Find("background").Find("itemName").GetComponent<Text>().text = itemName;
        shopItemTransform.Find("background").Find("itemCost").GetComponent<Text>().text = itemCost.ToString();

        shopItemTransform.Find("background").Find("itemSprite").GetComponent<Image>().sprite = itemSprite;

        shopItemTransform.GetComponent<Button_UI>().ClickFunc = () =>
        {
            //Clicked on shop item button 
            TryBuyItem(itemType);
            Debug.Log("Registered Click");
        };
    }

    private void TryBuyItem(Item.ItemType itemType)
    {
        equippedOrNotDic[itemType] = true;
        totalCoinAmount -= Item.GetCost(itemType);
        Debug.Log("Subtracted Required Amount");
        Debug.Log (totalCoinAmount);
    }

}
