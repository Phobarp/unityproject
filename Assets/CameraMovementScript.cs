using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovementScript : MonoBehaviour
{
    [SerializeField] private bool cameraShouldMove = true;
    public float cameraMoveSpeed = 0.1F;

    // Update is called once per frame
    void Update()
    {
        if (cameraShouldMove)
        {
            transform.position = new Vector3(transform.position.x + cameraMoveSpeed, transform.position.y, transform.position.z);
        }
    }
}
