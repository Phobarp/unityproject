using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{

  
    private const float Player_distance_spawn_Level_Part = 200f;
    [SerializeField] private Transform LevelPartStart;
    [SerializeField] private List<Transform> LevelPartList;
    [SerializeField] private Player player;

    private Vector3 LastEndPosition;
    private void Awake()
    {
        LastEndPosition = LevelPartStart.Find("EndPosition").position;
        int startingSpawnLevelParts = 5;
        for (int i = 0; i < startingSpawnLevelParts; i++)
        {
            SpawnLevelPart();
        }

    }

    private void Update()
    {
        if (Vector3.Distance(player.GetPosition(), LastEndPosition) < Player_distance_spawn_Level_Part)
        {
            SpawnLevelPart();
        }
    }
    private void SpawnLevelPart()
    {
        Transform chosenLevelPart = LevelPartList[Random.Range(0, LevelPartList.Count)];
        Transform LastlevelPartTransform = SpawnLevelPart(chosenLevelPart, LastEndPosition);
        LastEndPosition = LastlevelPartTransform.Find("EndPosition").position;
    }

    private Transform SpawnLevelPart(Transform LevelPart, Vector3 spawnPosition)
    {
        Transform levelPartTransform = Instantiate(LevelPart, spawnPosition, Quaternion.identity);
        return levelPartTransform;

    }
}
