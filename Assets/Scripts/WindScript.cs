using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class WindScript : MonoBehaviour
{
    [SerializeField] private float speed;

    internal Vector3 direction;
    private Rigidbody2D gameObjectBody;

    private void Start()
    {
        transform.position = new Vector3(0.5f, 0f, 0f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Obstacle" || collision.gameObject.tag == "NPC" || collision.gameObject.tag == "Player")
        {
            Vector2 forceDirection = new Vector2(direction.x, direction.y);
            gameObjectBody = collision.GetComponent<Rigidbody2D>();

            print(forceDirection);
            print(speed);

            gameObjectBody.AddForce(forceDirection * speed, ForceMode2D.Impulse);
        }
    }
}