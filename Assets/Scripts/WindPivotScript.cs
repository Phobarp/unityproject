using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindPivotScript : MonoBehaviour
{
    private Vector3 fingerDownPosition;
    private Vector3 fingerUpPosition;
    private bool windEnabled = false;
    [SerializeField] private float maxSwipeDistance;
    private bool finishedSwiping = false;

    private void Start()
    {
        GetComponentInChildren<BoxCollider2D>().enabled = false;
        GetComponentInChildren<SpriteRenderer>().enabled = false;
    }

    private float GetSwipeDistance()
    {
        float swipeDistance = Vector3.Distance(fingerDownPosition, fingerUpPosition);

        if (swipeDistance > maxSwipeDistance) { swipeDistance = maxSwipeDistance; } // Sets the swipe distance so that the wind does not extend too far

        return swipeDistance;
    }

    public void SendSwipeDirection()
    {
        Vector3 direction = (fingerUpPosition - fingerDownPosition).normalized;
        GetComponentInChildren<WindScript>().direction = direction;
    }

    private void ApplyWind(float windDistance)
    {
        transform.position = fingerDownPosition; // Positions wind sprite in the middle of the swipe so that the scaling of the sprite works

        // Rotates the sprite to align with the direction
        var delta = fingerUpPosition - transform.position;
        var angle = Mathf.Atan2(delta.y, delta.x) * Mathf.Rad2Deg;
        var rotation = Quaternion.Euler(0, 0, angle);
        transform.rotation = rotation;
        //Scales the sprite to fit the swipe distance and wind power width
        transform.localScale = new Vector3(windDistance, 3.5f, 0f);

        SendSwipeDirection();

        windEnabled = true;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            fingerDownPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z));
            finishedSwiping = false;
        }

        if (Input.GetMouseButtonUp(0))
        {
            fingerUpPosition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -Camera.main.transform.position.z));
            finishedSwiping = true;
        }

        if (finishedSwiping)
        {
            ApplyWind(GetSwipeDistance());
        }

        if (windEnabled)
        {
            GetComponentInChildren<BoxCollider2D>().enabled = true;
            GetComponentInChildren<SpriteRenderer>().enabled = true;
        }
    }
}
