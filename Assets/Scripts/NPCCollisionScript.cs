using UnityEngine;

public class NPCCollisionScript : MonoBehaviour
{
    public PlayerMovement movement;

	void OnCollisionEnter2D (Collision2D obstacle)
	{
		if (obstacle.collider.tag == "NPC")
		{
			Debug.Log("DEATH");
			movement.enabled = false;
		}
	}


}
