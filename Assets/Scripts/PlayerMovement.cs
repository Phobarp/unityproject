﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float defaultSpeed;
    internal float addedSpeed;
    internal bool playerShouldMove = true;
    public IEnumerator changeVelocityCoroutine;

    private void Start()
    {
        transform.position = new Vector3(-4, 1, 0);
    }

    void Update()
    {
        if (playerShouldMove) MovePlayer(getSpeed());
    }

    private float getSpeed() 
    {
        return defaultSpeed + addedSpeed;
    } 

    internal void MovePlayer(float xMovement) 
    {
        transform.Translate(new Vector2(xMovement, 0));
    }

    internal void ChangeVelocityForTime(float velocityChange, float time) 
    {
        IEnumerator coroutine = ChangeVelocityForTimeCoroutine(velocityChange, time);
        StartCoroutine(coroutine);
    }

    internal void ChangePlayerForTime(float time)
    {
        IEnumerator coroutine = StopPlayerForTimeCoroutine(time);
        StartCoroutine(coroutine);
    }

    private IEnumerator ChangeVelocityForTimeCoroutine(float velocityChange, float time) 
    {
        addedSpeed = velocityChange;
        yield return new WaitForSeconds(time);
        addedSpeed = 0;
    }

    private IEnumerator StopPlayerForTimeCoroutine(float time) 
    {
        playerShouldMove = false;
        yield return new WaitForSeconds(time);
        playerShouldMove = true;
    }
}
