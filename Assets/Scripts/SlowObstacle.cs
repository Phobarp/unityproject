using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowObstacle : MonoBehaviour
{
    PlayerMovement playerMovement;

    // Start is called before the first frame update
    void Start()
    {
        // Makes the object opaque for visual purposes
        Color color = this.GetComponent <SpriteRenderer> ().material.color;
        color.a = 0.5F;
        this.GetComponent<SpriteRenderer>().material.color = color;

        playerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
    }

    private void OnTriggerStay2D(Collider2D collisionInfo)
    {
        if (collisionInfo.gameObject.tag == "Player")
        {
            playerMovement.ChangeVelocityForTime(-1.5F, 3);
            Debug.Log("Hit detected on slow obstacle by player.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
