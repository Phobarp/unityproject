using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeSlowDown : MonoBehaviour
{
    [SerializeField] private float timeChange = .5F;
    [SerializeField] private float slowDownDuration = 4f;
    

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) && Time.timeScale == 1)
        {
            StartCoroutine(SlowDownTime());
        }
    }

    IEnumerator SlowDownTime()
    {
        Time.timeScale -= timeChange;
        yield return new WaitForSeconds(slowDownDuration);
        Time.timeScale += timeChange;
    }
}
