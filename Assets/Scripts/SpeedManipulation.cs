using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedManipulation : MonoBehaviour
{ 
    PlayerMovement playerMovement;
    [SerializeField] private float timeSpedUp;
    [SerializeField] private float speed;

    // Start is called before the first frame update
    void Start()
    {
        playerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            playerMovement.ChangeVelocityForTime(speed, timeSpedUp);
            Debug.Log("Speeding up Player for certain amount of time");
        }
    }
}

