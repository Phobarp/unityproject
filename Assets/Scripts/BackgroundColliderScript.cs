using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundColliderScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // Makes the object opaque for visual purposes
        GetComponent<MeshRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        var cameraPosition = Camera.main.transform.position;
        transform.position = new Vector3(cameraPosition.x, cameraPosition.y, 0);
    }
}
