using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VenusFlytrapPivot : MonoBehaviour
{
    public float playerChaseSpeed = 1F;
    public float defaultPositionReturnSpeed = 02.1F;

    private readonly bool touchingPlayer = false; // Should be edited to account for hitbox detection in the future, this is just a placeholder

    private Transform target;
    private Vector3 defaultPosition;

    private void Start()
    {
        SpawnVenusFlytrap();
    }

    void SpawnVenusFlytrap()
    {
        var xRandomPosition = Random.Range(-1.0F, 5.0F);
        defaultPosition = transform.position = new Vector2(xRandomPosition, 4F);

        var playerTransform= GameObject.FindGameObjectWithTag("Player").transform;
        target = playerTransform;
    }

    void LookAt()
    {

        var playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
        var delta = playerPosition - transform.position;
        var angle = Mathf.Atan2(delta.y, delta.x) * Mathf.Rad2Deg;
        var rotation = Quaternion.Euler(0, 0, angle);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime);

    }

    private bool IsPlayerWithinRange()
    {
        bool inRange = false;

        var playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
        var venusFlytrapPosition = transform.position;
        float distanceToPlayer = Vector3.Distance(venusFlytrapPosition, playerPosition);

        if (distanceToPlayer < 8.0)
        {
            inRange = true;
            //Debug.Log("Player in range");
        }

        return inRange;
    }



    private void ChangeFlytrapPosition()
    {
        bool playerInRange = IsPlayerWithinRange();
        // If player is in range and flytrap has not caught them, the flytrap extends towards the player
        if (playerInRange && (!touchingPlayer))
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, playerChaseSpeed/2);
            Debug.Log("Moved towards player.");
        }

        // If player is out of range and flytrap is extended, decrease scale value so that the flytrap returns to original position
        if (!playerInRange && (transform.position != defaultPosition))
        {
            transform.position = Vector3.MoveTowards(transform.position, defaultPosition, defaultPositionReturnSpeed);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ChangeFlytrapPosition();
        LookAt();
    }
}
