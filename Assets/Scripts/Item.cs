using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField] private ItemType[] itemName;
    [SerializeField] private Sprite[] itemSprites;
    private static Dictionary<ItemType, Sprite> nameSpriteDic = new Dictionary<ItemType, Sprite>();
    private static Dictionary<ItemType, bool> equippedOrNotDic = new Dictionary<ItemType, bool>();
    
    private void Start()
    {
        for (int i = 0; i < itemName.Length; i++)
        {
            nameSpriteDic.Add(itemName[i], itemSprites[i]);
        }

        
    }
    
    public enum ItemType 
    { 
    Armor, 
    Helmet
    }

    public static int GetCost(ItemType itemType)
    {
        switch (itemType)
        {
            default:
            case ItemType.Armor:    return 30;
            case ItemType.Helmet:   return 15;
        }
    }

    public static Sprite GetSprite(ItemType itemType)
    {
        return nameSpriteDic[itemType]; 
    }
}
