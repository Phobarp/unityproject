using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VenusFlytrap : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start()
    {
        SpawnVenusFlytrap();
    }

    private void OnTriggerEnter2D(Collider2D collisionInfo)
    {
        if (collisionInfo.gameObject.tag == "Player")
        {
            Debug.Log("Hit detected on flytrap by player.");
        }
    }

    public float playerChaseSpeed = 1.8F;
    public float defaultPositionReturnSpeed = 02.1F;

    private readonly bool touchingPlayer = false; // Should be edited to account for hitbox detection in the future, this is just a placeholder

    private Transform target;
    private Vector3 defaultPosition;

    void SpawnVenusFlytrap()
    {
        var xRandomPosition = Random.Range(-1.0F, 5.0F);
        defaultPosition = transform.position = new Vector2(xRandomPosition, 4F);

        var playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        target = playerTransform;
    }

    void LookAt()
    {

        var playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
        var delta = playerPosition - transform.position;
        var angle = Mathf.Atan2(delta.y, delta.x) * Mathf.Rad2Deg;
        var rotation = Quaternion.Euler(0, 0, angle);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime);

    }

    private bool IsPlayerWithinRange()
    {
        bool inRange = false;

        var playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
        var venusFlytrapPosition = transform.position;
        float distanceToPlayer = Vector3.Distance(defaultPosition, playerPosition);

        if (distanceToPlayer < 9.0)
        {
            inRange = true;
            //Debug.Log("Player in range");
        }

        return inRange;
    }



    private void ChangeFlytrapPosition()
    {
        bool playerInRange = IsPlayerWithinRange();
        // If player is in range and flytrap has not caught them, the flytrap extends towards the player
        if (playerInRange && (!touchingPlayer))
        {
            transform.position = Vector3.MoveTowards(transform.position, target.position, playerChaseSpeed / 50);
            
        }

        // If player is out of range and flytrap is extended, decrease scale value so that the flytrap returns to original position
        if (!playerInRange && (transform.position != defaultPosition))
        {
            transform.position = Vector3.MoveTowards(transform.position, defaultPosition, defaultPositionReturnSpeed / 50);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ChangeFlytrapPosition();
        LookAt();
    }
}
