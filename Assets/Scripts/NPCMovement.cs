using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMovement : MonoBehaviour
{
    private float min;
    private float max;
    [SerializeField] private float distance;
    [SerializeField] private float speed;
    void Start()
    {
        min = transform.position.x;
        max = transform.position.x + distance;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector2(Mathf.PingPong(Time.time * speed, max - min) + min, transform.position.y);
    }


}
