using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DigManipulation : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private Image digImage;
    [SerializeField] private float digTime;
    [SerializeField] private Vector2 digSize;
    [SerializeField] private PlayerMovement playerMovement;
    private float initialGravity;

    private bool isDigging = false;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) 
        {
            Dig();
        }
    }

    internal void Dig() 
    {
        initialGravity = player.GetComponent<Rigidbody2D>().gravityScale;

        if (isDigging || !PlayerIsOnGround()) return;

        isDigging = true;

        PrepareValues();
        InitializeDigImage();
        StartCoroutine(MovePlayer());
    }

    #region Initiate Dig Movement
    private IEnumerator MovePlayer()
    {
        float xAxisRadius = digImage.GetComponent<RectTransform>().sizeDelta.x * .68f / 2;
        float yAxisRadius = digImage.GetComponent<RectTransform>().sizeDelta.y / 1.5f;

        Vector3 playerStartingPosition = new Vector2(player.localPosition.x + digImage.GetComponent<RectTransform>().sizeDelta.x / 2, player.localPosition.y);
        float progress = 0;
        float fraction = Time.deltaTime * .5f / digTime;

        while (true)
        {
            player.localPosition = playerStartingPosition + new Vector3(EvaluateCurEllipsePoint(progress, xAxisRadius, yAxisRadius).x, EvaluateCurEllipsePoint(progress, xAxisRadius, yAxisRadius).y, player.localPosition.z);
            
            progress += fraction;
            digImage.fillAmount += fraction * 2;

            if (progress >= .5) break;

            yield return null;
        }
        isDigging = false;
        ResetValues();
    }

    private Vector2 EvaluateCurEllipsePoint(float progress, float xAxisRadius, float yAxisRadius)
    { 
        float angle = (Mathf.Deg2Rad * 360f * progress) + Mathf.PI / 2;
        float x = -Mathf.Sin(angle) * (xAxisRadius);
        float y = Mathf.Cos(angle) * (yAxisRadius);

        y = GravityFlipped() ? -y : y;

        return new Vector2(x, y);
    }

    #endregion

    #region Prepare and Reset
    private void PrepareValues() 
    {
        playerMovement.playerShouldMove = false;
        player.GetComponent<BoxCollider2D>().enabled = false;
        player.GetComponent<Rigidbody2D>().gravityScale = 0;
        transform.GetChild(0).gameObject.SetActive(true);
    }

    private void ResetValues() 
    {
        playerMovement.playerShouldMove = true;
        player.GetComponent<BoxCollider2D>().enabled = true;
        player.GetComponent<Rigidbody2D>().gravityScale = initialGravity;
    }
    #endregion

    #region Set Up Dig image
    private void InitializeDigImage() 
    {
        digImage.fillAmount = 0;

        Vector2 digPosition = GetDigPosition();

        digImage.GetComponent<RectTransform>().sizeDelta = digSize;
        digImage.GetComponent<RectTransform>().position = digPosition;
        digImage.GetComponent<Image>().fillClockwise = ShouldFillClockwise();
        digImage.GetComponent<RectTransform>().eulerAngles = GetImageRotation();
    }

    private Vector2 GetDigPosition() 
    {
        float digPositionY = GravityFlipped() ? digSize.y / 1.65f : -digSize.y / 1.65f;
        return new Vector2(player.position.x, player.position.y) + new Vector2(digSize.x / 2, digPositionY);
    }

    private Vector3 GetImageRotation() 
    {
        return GravityFlipped() ? Vector3.forward * 180 : Vector3.zero;
    }

    private bool ShouldFillClockwise() 
    {
        return GravityFlipped();
    }
    #endregion

    #region Helper Functions
    private bool PlayerIsOnGround()
    {
        float distanceToGround = player.GetComponent<BoxCollider2D>().bounds.extents.y;
        Vector2 raycastOrigin = GravityFlipped() ? new Vector2(player.position.x, player.position.y + distanceToGround) : new Vector2(player.position.x, player.position.y - distanceToGround);
        Vector2 raycastDirection = GravityFlipped() ? Vector2.up : Vector2.down;

        return Physics2D.Raycast(raycastOrigin, raycastDirection, .1f);
    }

    private bool GravityFlipped()
    {
        return Physics2D.gravity.y > 0;
    }

    #endregion
}
