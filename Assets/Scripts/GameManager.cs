﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private float distanceBetweenSpawns;
    [SerializeField] private Transform obstacleTemplates;
    [SerializeField] private Transform obstacleParent;
    private List<GameObject> obstacles = new List<GameObject>();
    private float currentSpawnX;
    private Vector3 playerPosition;

    public GameOverUI GameOverUI;

    private void Awake()
    {
        currentSpawnX = player.transform.position.x;

    }

    // Start is called before the first frame update
    void Start()
    {
        AddObstaclesToList();
        SpawnNewObstacle();
        SpawnNewObstacle();
    }

    private void Update()
    {
        playerPosition = Camera.main.WorldToViewportPoint(player.transform.position);

        if (ShouldSpawnNewObstacle()) 
        {
            SpawnNewObstacle();
        }

        if (playerPosition.x < 0.0)
        {
            GameOver();
        };
    }

    private void AddObstaclesToList()
    {
        foreach (Transform obstacle in obstacleTemplates) 
        {
            obstacles.Add(obstacle.gameObject);
        }
    }

    private void SpawnNewObstacle() 
    {
        Instantiate(GetRandomObstacle(), GetObsacleSpawnPoint(), new Quaternion(), obstacleParent);
        currentSpawnX += distanceBetweenSpawns;
    }

    private bool ShouldSpawnNewObstacle() 
    {
        if (player.transform.position.x >= currentSpawnX - distanceBetweenSpawns * 2) 
        {
            return true;
        }

        return false;
    }

    private Vector2 GetObsacleSpawnPoint() 
    {
        return new Vector3(currentSpawnX, player.transform.position.y, player.transform.position.y) + Vector3.right * distanceBetweenSpawns;
    }

    private GameObject GetRandomObstacle() 
    {
        int randomIndex = Random.Range(0, obstacles.Count - 1);
        return obstacles[randomIndex];
    }

    public void GameOver()
    {
        GameOverUI.Setup(0); // TODO: Score integer passing should be implemented once score system is implemented.
    }

    public void Restart() 
    {
        SceneManager.LoadScene("GameScene");
    }

    public void Home() 
    {
        SceneManager.LoadScene("Start screen");
    }
}
